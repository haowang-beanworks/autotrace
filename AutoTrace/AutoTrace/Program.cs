﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extras.DynamicProxy;
using Castle.Core.Logging;
using zipkin4net;
using zipkin4net.Tracers.Zipkin;
using zipkin4net.Transport.Http;

namespace AutoTrace {
    class Program {
        static void Main(string[] args) {
            var container = build();
            StartTrace();

            var service = container.Resolve<SyncService>();
            var trace = container.Resolve<Trace>();
            trace.Record(Annotations.LocalOperationStart("startup"));
            trace.Record(Annotations.ServiceName(System.Environment.MachineName));
            trace.Record(Annotations.Rpc("startup"));
            service.Synchronize(Guid.NewGuid(), DateTime.Today);
            service.Synchronize(Guid.NewGuid(), DateTime.Today);
            service.Another();

            trace.Record(Annotations.LocalOperationStop());

            StopTrace();
            Console.ReadLine();
        }

        static void StartTrace() {
            TraceManager.SamplingRate = 1.0f;
            TraceManager.RegisterTracer(new ZipkinTracer(new HttpZipkinSender("http://localhost:9411", "application/json"), new JSONSpanSerializer()));
            TraceManager.Start(new NullLogger());
        }

        static void StopTrace() {
            TraceManager.Stop();
            TraceManager.ClearTracers();
        }

        static IContainer build() {
            var builder = new ContainerBuilder();
            builder.Register(context => { return Trace.Create(); }).SingleInstance();
            builder.RegisterType<TraceInterceptor>();
            builder.RegisterType<SyncService>().SingleInstance().EnableClassInterceptors().InterceptedBy(typeof(TraceInterceptor));
            return builder.Build();
        }

        public class NullLogger : zipkin4net.ILogger {
            public void LogError(string message) {
                // void 
            }

            public void LogInformation(string message) {
                // void
            }

            public void LogWarning(string message) {
                // void
            }
        }
    }
}
