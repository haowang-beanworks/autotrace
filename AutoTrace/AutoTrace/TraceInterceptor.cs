﻿using System;
using Castle.DynamicProxy;

namespace AutoTrace {
    public class TraceInterceptor : IInterceptor {
        public void Intercept(IInvocation invocation) {
            Console.WriteLine("Start: " + invocation.TargetType.Name + "." + invocation.Method.Name);
            invocation.Proceed();
            Console.WriteLine("Finish: " + invocation.TargetType.Name + "." + invocation.Method.Name);
        }
    }
}