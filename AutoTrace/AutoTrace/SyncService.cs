﻿using System;
using Autofac.Features.Metadata;
using zipkin4net;

namespace AutoTrace {
    public class SyncService {
        private Trace trace;
        public SyncService(Trace trace) {
            this.trace = trace.Child();
            Meta<SyncService, SyncService> meta;
        }

        public virtual void Synchronize(Guid legalEntityId, DateTime? lastSyncDate) {
            trace.Record(Annotations.LocalOperationStart("SyncServcie.Synchronize"));
            trace.Record(Annotations.ServiceName("Sage100"));
            trace.Record(Annotations.Rpc("SyncServcie.Synchronize"));

            Console.WriteLine("SyncService.Synchronize() executing. ");

            trace.Record(Annotations.LocalOperationStop());
        }

        public virtual void Another() {
            trace.Record(Annotations.LocalOperationStart("SyncServcie.AnotherOperation"));
            trace.Record(Annotations.ServiceName("Sage100"));
            trace.Record(Annotations.Rpc("SyncServcie.Another"));

            Console.WriteLine("SyncService.Another() executing. ");

            trace.Record(Annotations.LocalOperationStop());
        }
    }
}